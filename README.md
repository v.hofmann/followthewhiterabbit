# FollowTheWhiteRabbit
"Follow the white rabbit - Guiding principles for increased efficiency and better communcation in Matrix projects." is an open document which is collaboratively developed by interested contributors thinking about leadership in matrix-organized projects. 

The document is intended to privide a set of guidance "rules" for project managers and line managers to prevent operational conflict in projects with matrix organizations. 

The 1st version of the document is the results of the Leadership insights (LSI) module of the Helmholtz Akademie für Führungskräfte - Führung übernehmen 2022

## rules for collaborations
In order to contribute: 
1. clone the project
1. create a new branch in which you commit your additions or changes to `FollowTheWhiteRabbit.md`
1. push the branch into a new merge request 
1. request review and merge by the initial authors. 

alternatively: 
1. read `WhiteRabbit-GuideToMoreEffectiveMatrixProjects.pdf`
1. create an issue on our [issue board](https://codebase.helmholtz.cloud/v.hofmann/followthewhiterabbit/-/issues) where you describe your addition changes in free-text
1. request review and merge by one of the moderators

## license
The document and all content in this repo is licensed under CC-BY-0 license
