<img src="https://codebase.helmholtz.cloud/v.hofmann/followthewhiterabbit/-/raw/main/Figures/Header_FTWR.png" alt="Header">

# Guiding principles for increased efficiency and better communication in Matrix projects
Leadership Insights (LSI) - Helmholtz Akademie für Führungskräfte - Führung übernehmen 2022


## Authors & Affiliation
- Christian Feiler, Helmholtz Zentrum Hereon 
https://orcid.org/0000-0003-4312-7629(https://orcid.org/0000-0003-4312-7629)
- Volker Hofmann, Forschungszentrum Jülich 
https://orcid.org/000-0002-5149-603X(https://orcid.org/000-0002-5149-603X)
- Isfried Petzenhauser, GSI Darmstadt 
- Barbara Schröder, Helmholtz Zentrum München
- Heike Siegler, Forschungszentrum Jülich


## Introduction 
Research and operations in the Helmholtz Association (HGF) heavily depend on the collaboration across groups, institutes, departments and research centers  This extends beyond the 18 research centers of the Helmholtz Association, for example to collaborations with the industry partners (i.e. in research and development) or the integration of external contractors in projects (i.e. in management, infrastructure development and maintenance, product development, business development etc.).

Such collaborative efforts are evident both in research itself as well as in supporting operations (e.g. administration, infrastructure, IT..) and have steadily increased in complexity over the years. Concomitantly, the organization of such collaborations have changed as well and to date, many projects in and around the HGF are organized in a matrix-like structure. 


### Key features of the Matrix Structure
A matrix structure is an organizational form characterized by multiple managerial accountability and responsibility levels (Fig.1). It typically comprises of parallel authoritative lines (Fig. 1, red), which in the HGF are often groups, institutes, or departments with a general or senior manager or scientist (Fig. 1, “SM”) acting as the disciplinary supervisor (Fig.1, red line) for a set of employees (Fig.1, “EM”). Orthogonal to that, projects may comprise a subset of EMs from different groups headed by an operations or project manager (PM) who holds responsibility for the operations of a given project (Fig. 1, blue).
Within the matrix of a given project, the originally set hierarchical relationships between these players may/will change entirely, up to a state where no formalized authoritative relationship between PM and SM exists.

<img src="https://codebase.helmholtz.cloud/v.hofmann/followthewhiterabbit/-/raw/main/Figures/Fig1.png" alt="Figure 1" width="500">

**Figure 1: The matrix structure.**
- SM = senior manager (disciplinary subervisor)
- EM = employee
- PM = project manager

### Benefits and disadvantages of the matrix – an excerpt
The benefit to organize collaborative projects in a matrix-like fashion are manifold since this form of organization comes with 
1. higher project agility, 
1. easier and more direct knowledge and technology transfer between all players, 
1. a more task-oriented distribution and efficient use of resources and, 
1. an increased opportunity for EMs to expand their skill sets.

However, the organization of collaborations in a matrix also creates a number of potential disadvantages and challenges in comparison to the classical authoritative organization since 
1. the varying authoritative relationship between a project’s PM, EM and SM often result in miscommunication and unclear responsibilities, 
1. conflicting goals of individual organizational units plus differences in their strategic plans create potential for mismatched priorities that negatively impact performance and output of the project, which in turn then results in 
1. a daily struggle for power and 
1. the performance and contributions of EMs may be hard to measure for their disciplinary superior who may not be part of a particular project. 

Overall, project organization in a matrix requires a substantial amount of communication between all participating stakeholders in order to overcome these challenges and harness the benefits of this form of organizational collaboration.

### The purpose of this document
We, the authors of this white paper were and still are part of different matrix settings in the varying roles (mostly as EMs and PMs). We have experienced both sides of the medallion:  the power of well organized collaborative projects and the hurdles that ill-managed and -structured projects entail. We faced the challenges and as such experienced all facets of such an endeavour first-hand.

We all attend the Helmholtz-Akademie für Führungskräfte as we are at more senior stage of our careers now and act more frequently in PM and SM roles. The leadership insights (LSI)-segment of the program “Führung übernehmen” therefore offers a great opportunity to reflect on our previous experience – for us to more consciously fulfill our roles in matrix-like organized collaborations and for others, our peers as well as the next generation of scientists, to raise awareness, learn from our experience and avoid common pitfalls right from the beginning wherever possible.

Our guidebook therefore summarizes conflict scenarios in matrix organized collaborations from different perspectives (SM, PM or EM) (Table. 1). As said before, these are derived from our own collective experience, i.e. we have previously witnessed or experienced these roadblocks in our collaborations, and we offer solutions and guidelines, that could help approaching and ideally solving such conflicts during ongoing operations.

We tried to distill a set of the most important guiding principles that “Leadership” (SMs and PMs alike) should be aware of when participating in matrix organized collaborations in our view. Our goal is to equip the reader with a toolbox that helps prevent conflicts from the start and boosts matrix collaborations efficiency and outcome. We are fully aware that these tools are not suitable for all projects since they all have a very individual character. Thus,the presented list of examples and guidelines is by no means comprehensive. 

Nevertheless, it covers a number of common themes, independent of the nature of the respective project and we believe that even the smallest mindfulness towards potential problems can facilitate the understanding of the different viewpoints of the participating parties and therefore prevent conflicts from arising in the first place.
We view this guidebook as a living document and we invite all readers to add, comment and complement its content as well as to distribute it within their professional networks inviting their peers to follow their example.


## Example scenarios of conflict
- LM: Line Manager (responsibility for staff)
- EM: Employee
- PM: Project Manager
- PO: Project Owner

| # | Problem  | Recommended Reaction | Preventative Measures at Setup |
| -- | -- | -- | -- |
| I | EM lacks expertise needed for project work | PM discusses with LM, LM takes measures to support EM‘s development or swaps project responsibilities within her/his team | Organisation builds a mandatory extra step into its project setup process: PM and LM discuss required expertise before staff is allocated |
| II | 2 EMs within a project do not get along, inhibiting their work | Go back to Storming & Forming | Allow time for Storming & Forming in initial project phase |
| III | LM pulls EM from project  | | Before project start, PM and LM agree on clear goals and time allocation for each EM involved, taking EM‘s own estimates into consideration. The Result is an internal „cooperation contract“ between line management and project. |
| IV | PO oblivious to real world problems | PM involves PO in more frequent meetings (shorter intervals than Review Meetings) to discuss goals and progress with everyone involved.  | PM involves PO in regular meetings to discuss goals and progress with everyone involved.  |
| V | Project Plan needs to be adjusted | PM communicates to EMs and LMs as early as possible | Agreement betwenn LM and PM that „cooperation contract“ can be renegotiated if circumstances change |
| VI | LM overloads EM with projects | EM raises the issue to their LM, involves PMs if problem persists | Organisation sets a minimum percentage contribution of EMs to projects to avoid LMs spreading their EMs too thin.  |Percentage depends on type of task. |
| VII | PM does not have the necessary skills for the role | LM strengthens PM by clearly communicating her/his role to all involved | In larger projects, include external PM (or external coach for internal PM) in grant proposal |
| VIII | EM leaves the organisation for reasons unrelated to the project | Shit happens ;-)  |
| IX | EM lacks motivation for project work | PM approaches LM, who works with EM to identify causes and set goals |  Clear and realistic role description during recruitment process |

## Guiding principles for navigating the Matrix

| Abbreviation | Role | Description |
| -- | -- | -- |
| PM  | Project Manager | Manages the project and directs/coordinates EMs across different groups, departments or institutes. |
| EM | Employee | is part of a group/ institute/department with and SM as the authoritative superior and collaborates in a project orthogonal to this authoritative line with EMs from other groups that are led by a PM  |
| SM | Senior Manager | acts as the authoritative superior of EM; is often involved in a project during planning & writing the proposal |

With the following guiding principles we intend to reach out to SMs and PMs operating in matrix- like organized collaborations as the main target group. The following list of guidelines are non-authoritative and instead intended to raise awareness. 

### (A) When planning the matrix project (i.e. writing the proposal)
- _**Create strategic synergy:**_ 
Make sure that your contribution to the project is well aligned with your own strategic interest in order to exploit synergies. If you plan to participate in a project in order to benefit from knowledge transfer, you should plan for capacities to realize these (i.e. recruiting or appointing an EM that does have all necessary expertise is a possibility, but should be clearly communicated with other SMs and the PM in order to allow for sufficient time to build expertise in the required field). 
- _**Contribute your expertise:**_
When planning the contribution to a matrix project be sure that one of your group members (i.e. the one who you would insert as EM into the matrix) has all the required expertise, or that you will be able to recruit a person fulfilling these criteria.
- _**Plan for communication effort:**_
Be aware that projects organized as matrices require considerable and intense communication efforts when projects are running. Therefore, participating in various matrices surpassing your communication capacity should be avoided.
Plan for regular and comprehensive reviews of project strategy and operations. This could be a quarterly “retreat” between selected EMs, the PM and all SMs and a annual “retreat” involving everyone. Often the time for such feedback-loops is heavily underestimated eventually resulting in a strong disparity between expectation towards SM participation in them and the actual participation possible.
- _**Don’t overload your employee:**_
Only plan for a contribution (i.e. % FTE) that an appointed employee can realistically contribute to a project.

### (B) Recruiting strategy
- _**Recruit a PM first:**_
The project manager (PM) should be hired as soon as possible
- _**Recruit as early as possible:**_
If you need to hire new EMs to contribute to a project, start recruiting efforts as early as possible and not only after the project start. Open positions increase non-budgeted funds, delay project operations and require the PM to readjust project contributions. This all creates additional and unnecessary workload on your PM that he could spend on creating output with your EMs. 
- _**Include the expertise of your PM:**_
Where possible, the PM should be included in the recruiting activities for the project. This can help to ensure that both PM and superior (SP) agree with respect to the profile and capabilities of the hired employee.
When appointing a current EM, consult your PM and discuss (maybe together with EM) if and how the (level of) expertise matches expected contributions, outcomes and tangible results.
- _**Contribute coherent pieces, not random crumbs:**_
For SM/PM: Be aware of the fact that splitting positions (i.e. 3x 33% FTE instead of 1x 100% FTE) can have the potential benefit of covering more and different fields of expertise

### (C) Ongoing operations
- _**Re-evaluate strategic alignment:**_
For SM: if an misalignment between the strategic direction of the project and your own group occurs, communicate that with the PM and the EM in order to resolve conflicts and avoid the EM having to balance different directives.
Should the strategic direction of the group, department or institute change and as a result become partially misaligned with a given project, communication with the PM should be sought in order to evaluate if and how that can be reflected in a meaningful way in the project contribution.
- _**Trust and support your PM and your EM:**_
For SM: It can be very difficult for SMs to evaluate the productivity and or judge the workload of an EM who is participating in a project outside of the SMs daily business. Here, continuous communication to both the EM and PM are required. Further, the EM should have the time appointed to a project to actually work on that project. Trust and support the self-organization skills of your EM, communicate that you are open to jointly reviewing workload of project contribution, should that be required from the perspective of the EM.
- _**Provide a good working environment:**_
For SM: Be aware of the fact that you are responsible for the working environment of your EM even if you’re not directly responsible for the operations of the EM under such circumstances (which in a project is realized through the PM). 
- _**Don’t interfere with operations directly:**_
For SM: Be aware the that PM might carry out operations different that you would. Delegate the  necessary authority to the PM to guide the project productively and clearly (!) communicate this to all parties involved. Voice concerns in a productive fashion and communicate them with the person concerned directly (i.e. it can create mistrust and conflicts communicating with the EM that the directives of the PM are misaligned with your own views).
- _**Don’t moderate above the PMs head:**_
Don’t try to solve or moderate conflicts from “the outside” – communicate with PM if conflicts between EM or SM and PM exist and aim at a constructive resolution of these. 
- _**Include all EMs:**_
For PM: create an inclusive environment involving all EMs in a similar fashion, make every voice heard and show your appreciations towards the EMs contributions to the project. Unbalanced scales (i.e. in responsibilities, credits or else) are a common source of conflict in a matrix.
- _**Distribute and communicate responsibilities clearly:**_
For PM: create, allocate and communicate clear responsibilities within the project. 
Invest in (joint) documentation and communication. Only clearly communicating responsibility to EM and being sure that this is in line with what SM are expecting will results in constructive output. 
